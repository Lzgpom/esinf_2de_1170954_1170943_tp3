package esinf.tp3.io;

import esinf.tp3.model.MorseCharacter;
import esinf.tp3.model.MorseCode;
import esinf.tp3.model.impl.MorseCharacterImpl;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class FileImporterTest
{
    @Test
    void ensureLoadMorseCodeIsCorrect()
    {
        MorseCharacter c1 = new MorseCharacterImpl("E", ".", MorseCharacter.Type.LETTER);
        MorseCharacter c2 = new MorseCharacterImpl("T", "_", MorseCharacter.Type.LETTER);
        MorseCharacter c3 = new MorseCharacterImpl("I", "..", MorseCharacter.Type.LETTER);
        MorseCharacter c4 = new MorseCharacterImpl("", "", "");

        String file = "target/test-classes/morse.csv";

        MorseCode result = FileImporter.loadMorseCode(file);
        Iterator<MorseCharacter> characters = result.getCharacters().iterator();

        assertEquals(c3, characters.next());
        assertEquals(c1, characters.next());
        assertEquals(c4, characters.next());
        assertEquals(c2, characters.next());
    }

    @Test
    void ensureLoadMorseCodeReturnsNullWhenFileNotFound()
    {
        MorseCode result = FileImporter.loadMorseCode("somewhere");
        assertEquals(null, result);
    }
}