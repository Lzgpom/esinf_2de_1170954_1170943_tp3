package esinf.tp3.utils.impl;

import java.util.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BSTImplTest
{
    private Integer[] arr = {20, 15, 10, 13, 8, 17, 40, 50, 30, 7};
    private int[] height = {0, 1, 2, 3, 3, 3, 3, 3, 3, 4};
    private Integer[] inorderT = {7, 8, 10, 13, 15, 17, 20, 30, 40, 50};
    private Integer[] preorderT = {20, 15, 10, 8, 7, 13, 17, 40, 30, 50};
    private Integer[] posorderT = {7, 8, 13, 10, 17, 15, 30, 50, 40, 20};

    BSTImpl<Integer> instance;

    BSTImplTest()
    {
    }

    @BeforeEach
    void setUp()
    {
        instance = new BSTImpl<>();
        for (int i : arr)
            instance.insert(i);
    }

    /**
     * Test of size method, of class BST.
     */
    @Test
    void testSize()
    {
        assertEquals(instance.size(), arr.length);

        BSTImpl<String> sInstance = new BSTImpl<>();
        assertEquals(sInstance.size(), 0);
        sInstance.insert("A");
        assertEquals(sInstance.size(), 1);
        sInstance.insert("B");
        assertEquals(sInstance.size(), 2);
        sInstance.insert("A");
        assertEquals(sInstance.size(), 2);
    }

    /**
     * Test of insert method, of class BST.
     */
    @Test
    void testInsert()
    {
        int arr[] = {20, 15, 10, 13, 8, 17, 40, 50, 30, 20, 15, 10};
        BSTImpl<Integer> instance = new BSTImpl<>();
        for (int i = 0; i < 9; i++)
        {            //new elements
            instance.insert(arr[i]);
            assertEquals(instance.size(), i + 1);
        }
        for (int i = 9; i < arr.length; i++)
        {    //duplicated elements => same size
            instance.insert(arr[i]);
            assertEquals(instance.size(), 9);
        }
    }

    /**
     * Test of isEmpty method, of class BST.
     */
    @Test
    void testIsEmpty()
    {
        BSTImpl<Integer> result = new BSTImpl<>();
        assertTrue(result.isEmpty());
    }

    @Test
    void ensureFindIsCorrect()
    {
        int expected = 8;
        int result = instance.find(8);
        assertEquals(expected, result);
    }

    @Test
    void ensureBFSIsCorrect()
    {
        BSTImpl<Integer> bst = new BSTImpl<>();
        bst.insert(2);
        bst.insert(3);
        bst.insert(1);

        Iterator<Integer> iterator = bst.bfsOrder().iterator();

        assertEquals(2, iterator.next().intValue());
        assertEquals(1, iterator.next().intValue());
        assertEquals(3, iterator.next().intValue());
    }


    /**
     * Test of inOrder method, of class BST.
     */
    @Test
    void testInOrder()
    {
        List<Integer> lExpected = Arrays.asList(inorderT);
        assertEquals(lExpected, instance.inOrder());
    }
}
