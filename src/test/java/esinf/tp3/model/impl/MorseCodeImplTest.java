package esinf.tp3.model.impl;

import esinf.tp3.io.FileImporter;
import esinf.tp3.model.MorseCharacter;
import esinf.tp3.model.MorseCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MorseCodeImplTest
{
    MorseCodeImpl codeEmpty;
    MorseCode code;

    @BeforeEach
    void setup()
    {
        codeEmpty = new MorseCodeImpl();
        code = FileImporter.loadMorseCode("target/test-classes/morseFull.csv");
    }

    @Test
    void ensureInsertAndGetCharacterWorks()
    {
        codeEmpty.insertCharacter(new MorseCharacterImpl("E", ".", MorseCharacter.Type.LETTER));

        MorseCharacter expected = new MorseCharacterImpl("E", ".", MorseCharacter.Type.LETTER);
        MorseCharacter result = codeEmpty.getCharacters().iterator().next();
        assertEquals(expected, result);
    }

    @Test
    void ensureDecodeMorseCodeIsCorrect()
    {
        String expected = "ESINF";
        String result = code.decodeMorseCode(". ... .. _. .._.");
        assertEquals(expected, result);
    }

    @Test
    void ensureGetOnlyLetterMorseCodeIsCorrect()
    {
        codeEmpty.insertCharacter(new MorseCharacterImpl("E", ".", MorseCharacter.Type.LETTER));
        codeEmpty.insertCharacter(new MorseCharacterImpl("4", "....", MorseCharacter.Type.NUMBER));

        MorseCode result = codeEmpty.getOnlyLetterMorseCode();

        MorseCharacter expected = new MorseCharacterImpl("E", ".", MorseCharacter.Type.LETTER);
        Iterator<MorseCharacter> iterator = result.getCharacters().iterator();

        assertEquals(expected, iterator.next());
        assertEquals(new MorseCharacterImpl("", "", ""), iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    void ensureDecodeMorseCodeOnlyLetters()
    {
        String expected = "ESINF";
        String result = code.decodeMorseCodeOnlyLetters(". ... .. _. .._.");
        assertEquals(expected, result);
    }

    @Test
    void ensureOrderListOfWordsByTypeIsCorrectLetter()
    {
        LinkedList<String> expected = new LinkedList<>();
        expected.add("ABC");
        expected.add("AB3");
        expected.add("A23");
        expected.add("!2C");
        expected.add("!$C");

        List<String> words = new ArrayList<>();
        words.add(".____ ..___ ...__ ");
        words.add("._ ..___ ...__");
        words.add("._ _... ...__");
        words.add("._ _... _._.");
        words.add("_._.__ ..___ _._.");
        words.add("_._.__ ..._.._ _._.");
        words.add("_._.__ ..._.._ _..._");

        LinkedList<String> result = code.orderListOfWordsByType(words, MorseCharacter.Type.LETTER);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrderListOfWordsByTypeIsCorrectNumber()
    {
        LinkedList<String> expected = new LinkedList<>();
        expected.add("123");
        expected.add("A23");
        expected.add("AB3");
        expected.add("!2C");

        List<String> words = new ArrayList<>();
        words.add(".____ ..___ ...__ ");
        words.add("._ ..___ ...__");
        words.add("._ _... ...__");
        words.add("._ _... _._.");
        words.add("_._.__ ..___ _._.");
        words.add("_._.__ ..._.._ _._.");
        words.add("_._.__ ..._.._ _..._");

        LinkedList<String> result = code.orderListOfWordsByType(words, MorseCharacter.Type.NUMBER);
        assertEquals(expected, result);
    }

    @Test
    void ensureGetBeginningSequenceCommonIsCorrect()
    {
        String expected = "";
        String result = code.getBeginningSequenceCommon("...", "_");
        assertEquals(expected, result);
    }

    @Test
    void ensureGetBeginningSequenceCommonIsCorrect1()
    {
        String expected = ".";
        String result = code.getBeginningSequenceCommon("...", "._.");
        assertEquals(expected, result);
    }

    @Test
    void ensureGetBeginningSequenceCommonIsCorrect2()
    {
        String expected = "___";
        String result = code.getBeginningSequenceCommon("___.", "______");
        assertEquals(expected, result);
    }

    @Test
    void ensureCodeMorseCodeOnlyLetterIsCorrect()
    {
        String expected = ". ... .. _. .._.";
        String result = code.codeMorseCodeOnlyLetters("ESINF");
        assertEquals(expected, result);
    }

}