package esinf.tp3.model.impl;

import esinf.tp3.model.MorseCharacter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MorseCharacterImplTest
{
    MorseCharacterImpl character;

    @BeforeEach
    void setUp()
    {
        character = new MorseCharacterImpl("A", "._", "Letter");
    }

    @Test
    void ensureGetCharacterIsCorrect()
    {
        String expected = "A";
        String result = character.getCharacter();
        assertEquals(expected, result);
    }

    @Test
    void ensureGetMorseCodeIsCorrect()
    {
        String expected = "._";
        String result = character.getMorseCode();
        assertEquals(expected, result);
    }

    @Test
    void ensureGetTypeIsCorrect()
    {
        MorseCharacter.Type expected = MorseCharacterImpl.Type.LETTER;
        MorseCharacter.Type result = character.getType();
        assertEquals(expected, result);
    }

    @Test
    void ensureCompareToBiggerOtherIsCorrect()
    {
        MorseCharacter other = new MorseCharacterImpl(".__");
        assertTrue(character.compareTo(other) < 0);
    }

    @Test
    void ensureCompareToSmallerOtherIsCorrect()
    {
        MorseCharacter other = new MorseCharacterImpl("._.");
        assertTrue(character.compareTo(other) > 0);
    }

    @Test
    void ensureCompareToBiggerLengthOtherIsCorrect()
    {
        MorseCharacter other = new MorseCharacterImpl(".");
        assertTrue(character.compareTo(other) > 0);
    }

    @Test
    void ensureCompareToSmallerLengthOtherIsCorrect()
    {
        MorseCharacter other = new MorseCharacterImpl("_");
        assertTrue(character.compareTo(other) < 0);
    }

    @Test
    void ensureCompareToSameLengthOtherIsCorrect()
    {
        MorseCharacter other = new MorseCharacterImpl("_.");
        assertTrue(character.compareTo(other) < 0);
    }

    @Test
    void ensureHashCodeIsCorrect()
    {
        int expected = 3536;
        int result = character.hashCode();
        assertEquals(expected, result);
    }

    @Test
    void ensureEqualsReturnsFalseWhenNull()
    {
        boolean result = character.equals(null);
        assertFalse(result);
    }

    @Test
    void ensureEqualsReturnsFalseWhenNotSameType()
    {
        boolean result = character.equals(new Object());
        assertFalse(result);
    }

    @Test
    void ensureEqualsReturnFalseDifferentCharacter()
    {
        boolean result = character.equals(new MorseCharacterImpl("E", "._", "Letter"));
        assertFalse(result);
    }

    @Test
    void ensureEqualsReturnFalseDifferentCode()
    {
        boolean result = character.equals(new MorseCharacterImpl("A", ".__", "Letter"));
        assertFalse(result);
    }

    @Test
    void ensureEqualsReturnFalseDifferentType()
    {
        boolean result = character.equals(new MorseCharacterImpl("A", "._", "Number"));
        assertFalse(result);
    }

    @Test
    void ensureEqualsReturnTrue()
    {
        boolean result = character.equals(new MorseCharacterImpl("A", "._", "Letter"));
        assertTrue(result);
    }
}