package esinf.tp3;

import esinf.tp3.io.FileImporter;
import esinf.tp3.model.MorseCharacter;
import esinf.tp3.model.MorseCode;

import java.util.ArrayList;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        MorseCode code = FileImporter.loadMorseCode(FileImporter.MORSE_CODE_FILE);

        for(MorseCharacter character : code.getCharacters())
        {
            printCharacter(character);
        }

        String tmp = "... ___ ...";
        System.out.println(code.decodeMorseCode(tmp));
        System.out.println(code.decodeMorseCodeOnlyLetters(tmp));

        List<String> words = new ArrayList<>();
        words.add(".____ ..___ ...__ ");
        words.add("._ ..___ ...__");
        words.add("._ _... ...__");
        words.add("._ _... _._.");
        words.add("_._.__ ..___ _._.");
        words.add("_._.__ _.._. _._.");
        words.add("_._.__ _.._. _..._");

        System.out.println(code.orderListOfWordsByType(words, MorseCharacter.Type.LETTER));
        System.out.println(code.orderListOfWordsByType(words, MorseCharacter.Type.NUMBER));

        System.out.println(code.codeMorseCodeOnlyLetters("ESINF"));

        System.out.println(code.getBeginningSequenceCommon("....", ".._."));
    }

    private static void printCharacter(MorseCharacter character)
    {
        System.out.println(character.getCharacter() + " " + character.getMorseCode() + " " + character.getType());
    }
}
