package esinf.tp3.io;

import esinf.tp3.model.MorseCharacter;
import esinf.tp3.model.MorseCode;
import esinf.tp3.model.impl.MorseCharacterImpl;
import esinf.tp3.model.impl.MorseCodeImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileImporter
{
    public static final String MORSE_CODE_FILE = "morse.csv";
    private static final String FILE_REGEX = " ";

    /**
     * Loads the file with the morse code.
     * @return A instance of {@link MorseCode} with the information read. If the file is not found returns null.
     */
    public static MorseCode loadMorseCode(String file)
    {
        try
        {
            MorseCode out = new MorseCodeImpl();

            Scanner input = new Scanner(new File(file));

            while(input.hasNext())
            {
                //param[0] should be the code.
                //param[1] should be the character.
                //param[2] should be the type of character.
                String[] params = input.nextLine().split(FILE_REGEX);
                MorseCharacter character = new MorseCharacterImpl(params[1], params[0], params[2]);
                out.insertCharacter(character);
            }

            input.close();
            return out;
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File not found while reading the stations.");
        }

        return null;
    }
}
