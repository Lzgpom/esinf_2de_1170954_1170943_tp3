package esinf.tp3.utils;

import java.util.List;
import java.util.Map;

public interface BST<E> {

    boolean isEmpty();
    void insert(E element);
    void remove(E element);

    int size();
    int height();
    
    E smallestElement();
    E find(E element);
    Iterable<E> inOrder();
    Iterable<E> bfsOrder();
    Iterable<E> preOrder();
    Iterable<E> posOrder();
    Map<Integer,List<E>> nodesByLevel();
}
