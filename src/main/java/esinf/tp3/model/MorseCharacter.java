package esinf.tp3.model;

/**
 * This class represents a Character with a Morse Code
 * and the type of character it is.
 */
public interface MorseCharacter extends Comparable<MorseCharacter>
{
    /**
     * All the types of characters that exist.
     */
    enum Type
    {
        LETTER,
        NUMBER,
        NON_ENGLISH,
        PUNCTUATION,
        PROSIGN;

        /**
         * Returns the {@link Type type} of char given its description.
         * <br>"Letter" -> LETTER
         * @param type The description of the type of char.
         * @return The type of the char, if the type is invalid returns null.
         */
        public static Type getType(String type)
        {
            if(type.equals("Letter"))
            {
                return LETTER;
            }

            if(type.equals("Number"))
            {
                return NUMBER;
            }

            if(type.equals("Non-English"))
            {
                return NON_ENGLISH;
            }

            if(type.equals("Punctuation"))
            {
                return PUNCTUATION;
            }
            if(type.equals("Prosign"))
            {
                return PROSIGN;
            }

            return null;
        }
    }

    /**
     * @return The character of this.
     */
    String getCharacter();

    /**
     * Returns the morse code of this character in a String.
     * <br>Example: 'J' -> ".___"
     * @return The morse code of the character.
     */
    String getMorseCode();

    /**
     * Returns the type of the character.
     * <br> Examples
     * <br>'A' -> Letter
     * <br>'1' -> Number
     * <br>'Ã' -> Non-English
     * @return The type of character.
     */
    Type getType();
}
