package esinf.tp3.model.impl;

import esinf.tp3.model.MorseCharacter;

/**
 * This is just a simple implementation.
 */
public class MorseCharacterImpl implements MorseCharacter
{
    private String c;
    private String code;
    private Type type;

    /**
     * Creates a Character. It is used just to find the character
     * of this morse code in a BST.
     * @param code The morse code.
     */
    public MorseCharacterImpl(String code)
    {
        this.c = "";
        this.code = code;
        this.type = null;
    }

    /**
     * Creates a Character.
     * @param c The character itself.
     * @param code The morse code.
     * @param type The type description of character.
     */
    public MorseCharacterImpl(String c, String code, String type)
    {
        this.c = c;
        this.code = code;
        this.type = Type.getType(type);
    }

    /**
     * Creates a Character.
     * @param c The character itself.
     * @param code The morse code.
     * @param type The type of character.
     */
    public MorseCharacterImpl(String c, String code, Type type)
    {
        this.c = c;
        this.code = code;
        this.type = type;
    }

    @Override
    public String getCharacter()
    {
        return c;
    }

    @Override
    public String getMorseCode()
    {
        return code;
    }

    @Override
    public Type getType()
    {
        return type;
    }

    @Override
    public int compareTo(MorseCharacter other)
    {
        if(code.length() == other.getMorseCode().length())
        {
            return code.compareTo(other.getMorseCode());
        }

        //The minimum size of both codes
        int max = (code.length() < other.getMorseCode().length()) ? code.length() : other.getMorseCode().length();

        //The result of the comparison between the substrings of the codes with the same length.
        //Example: this ->".._"
        //         other ->"_"
        //         In this case it would compare "." with "_".
        int compare = code.substring(0, max).compareTo(other.getMorseCode().substring(0, max));

        //If the substrings are equal it checks the next char of the longest string.
        if(compare == 0)
        {
            if(code.length() < other.getMorseCode().length())
            {
                char tmp = other.getMorseCode().charAt(code.length());

                return (tmp > '.') ? -1 : 1;
            }

            char tmp = code.charAt(other.getMorseCode().length());

            return (tmp > '.') ? 1 :- 1;
        }

        return compare;
    }

    @Override
    public int hashCode()
    {
        int hash = c.hashCode();
        return hash * 31 + code.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
        {
            return false;
        }

        if(!(obj instanceof MorseCharacterImpl))
        {
            return false;
        }

        MorseCharacterImpl other = (MorseCharacterImpl) obj;

        return this.c.equals(other.c) && this.code.equals(other.code) && this.type == other.type;
    }
}
