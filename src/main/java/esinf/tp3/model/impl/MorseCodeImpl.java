package esinf.tp3.model.impl;

import esinf.tp3.model.MorseCharacter;
import esinf.tp3.model.MorseCode;
import esinf.tp3.utils.impl.BSTImpl;

import java.util.*;

/**
 * This is an implementation of {@link MorseCode morse code}.
 * It uses a Binary Search Tree.
 * @see esinf.tp3.utils.BST
 */
public class MorseCodeImpl implements MorseCode
{
    private BSTImpl<MorseCharacter> bst;

    /**
     * Creates an empty Morse code.
     */
    public MorseCodeImpl()
    {
        bst = new BSTImpl<>();
        insertCharacter(new MorseCharacterImpl("", "", ""));
    }

    @Override
    public void insertCharacter(MorseCharacter character)
    {
        bst.insert(character);
    }

    @Override
    public Iterable<MorseCharacter> getCharacters()
    {
        return bst.inOrder();
    }

    /**
     * Decodes a morse code into a word.
     * @param code The morse code to decode.
     * @return A {@link MorseWord word}.
     */
    private MorseWord decode(String code)
    {
        String[] letterCodes = code.split(" ");

        MorseWord word = new MorseWord();


        for(String letter : letterCodes)
        {
            MorseCharacter character = bst.find(new MorseCharacterImpl(letter));
            word.addMorseCharacter(character);
        }

        return word;
    }

    @Override
    public String decodeMorseCode(String code)
    {
        return decode(code).getWord();
    }

    @Override
    public String decodeMorseCodeOnlyLetters(String code)
    {
        MorseCode morseCode = getOnlyLetterMorseCode();
        return morseCode.decodeMorseCode(code);
    }

    @Override
    public String codeMorseCodeOnlyLetters(String word)
    {
        MorseCodeImpl morseCode = (MorseCodeImpl) getOnlyLetterMorseCode();

        MorseWord morseWord = new MorseWord();

        for(char character : word.toCharArray())
        {
            morseWord.addMorseCharacter(getMorseCharacter(character + "", morseCode.bst.root()));
        }

        return morseWord.getMorseCode();
    }

    /**
     * Searches the bst for the character and returns a {@link MorseCharacter}.
     * @param character The character to search for.
     * @param node The node that it is in.
     * @return The {@link MorseCharacter} if found else null.
     */
    private MorseCharacter getMorseCharacter(String character, BSTImpl.Node<MorseCharacter> node)
    {
        if(node.getElement().getCharacter().equals(character))
        {
            return node.getElement();
        }

        if(node.getLeft() != null)
        {
            MorseCharacter out = getMorseCharacter(character, node.getLeft());

            if(out != null)
            {
                return out;
            }
        }

        if(node.getRight() != null)
        {
            MorseCharacter out = getMorseCharacter(character, node.getRight());

            if(out != null)
            {
                return out;
            }
        }

        return null;
    }

    @Override
    public LinkedList<String> orderListOfWordsByType(List<String> morseWords, MorseCharacter.Type type)
    {
        LinkedList<MorseWord> words = new LinkedList<>();

        for(String wordToDecode : morseWords)
        {
            MorseWord morseWord = decode(wordToDecode);

            if(morseWord.getNumberOfCharactersOfType(type) != 0)
            {
                words.add(morseWord);
            }
        }

        words.sort(Collections.reverseOrder(Comparator.comparingInt(o -> o.getNumberOfCharactersOfType(type))));

        LinkedList<String> out = new LinkedList<>();

        for(MorseWord word : words)
        {
            out.add(word.getWord());
        }

        return out;
    }

    @Override
    public MorseCode getOnlyLetterMorseCode()
    {
        MorseCode out = new MorseCodeImpl();

        for(MorseCharacter character : bst.bfsOrder())
        {
            if(character.getType() == MorseCharacter.Type.LETTER)
            {
                out.insertCharacter(character);
            }
        }

        return out;
    }

    @Override
    public String getBeginningSequenceCommon(String code1, String code2)
    {
        MorseCharacter char1 = new MorseCharacterImpl(code1);
        MorseCharacter char2 = new MorseCharacterImpl(code2);

        BSTImpl.Node<MorseCharacter> root =  bst.root();
        int num = getNumSequence(char1, char2, root, 0);

        return code1.substring(0, num);
    }

    /**
     * This is a recursive method, that counts the number of characters of
     * the code that are the same between two {@link MorseCharacter morse characters}.
     * @param char1 A morse character.
     * @param char2 Another morse character.
     * @param node The node that it is.
     * @param num The count of the sequence with same chars.
     * @return The number of characters of the code that are the same
     *         between two {@link MorseCharacter morse characters}.
     */
    private int getNumSequence(MorseCharacter char1, MorseCharacter char2, BSTImpl.Node<MorseCharacter> node, int num)
    {
        if(node == null)
        {
            return num;
        }

        if(char1.compareTo(node.getElement()) < 0 && char2.compareTo(node.getElement()) < 0)
        {
            return getNumSequence(char1, char2, node.getLeft(), num + 1);
        }

        if(char1.compareTo(node.getElement()) > 0 && char2.compareTo(node.getElement()) > 0)
        {
            return getNumSequence(char1, char2, node.getRight(), num + 1);
        }

        return num;
    }

    /**
     * This class represents a word with only Morse Characters.
     */
    private class MorseWord
    {
        LinkedList<MorseCharacter> word;

        /**
         * Creates an empty instance of MorseWord.
         */
        MorseWord()
        {
            this.word = new LinkedList<>();
        }

        /**
         * Adds a {@link MorseCharacter character} to the word.
         * @param character The {@link MorseCharacter character} to add.
         */
        void addMorseCharacter(MorseCharacter character)
        {
            this.word.add(character);
        }

        /**
         * Converts this morse word in a String form.
         * @return The word in String mode.
         */
        String getWord()
        {
            StringBuilder s = new StringBuilder();

            for(MorseCharacter letter : word)
            {
                s.append(letter.getCharacter());
            }

            return s.toString();
        }

        /**
         * Converts this morse word in a String form of the morse code.
         * @return The word in String mode.
         */
        String getMorseCode()
        {
            StringBuilder s = new StringBuilder();

            for(int i = 0; i < word.size(); i++)
            {
                s.append(word.get(i).getMorseCode());

                if(i < word.size() - 1)
                {
                    s.append(" ");
                }
            }

            return s.toString();
        }

        /**
         * Calculates the number of characters of a certain type.
         * @param type The {@link MorseCharacter.Type type} to look for.
         * @return The number of characters of that type found.
         */
        int getNumberOfCharactersOfType(MorseCharacter.Type type)
        {
            int total = 0;

            for(MorseCharacter character : word)
            {
                if(character.getType() == type)
                {
                    total++;
                }
            }

            return total;
        }
    }
}
