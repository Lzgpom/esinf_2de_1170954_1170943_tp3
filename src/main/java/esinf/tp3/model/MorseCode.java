package esinf.tp3.model;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents all the morse code.
 */
public interface MorseCode
{
    /**
     * Inserts a {@link MorseCharacter character} in the morse code.
     * @param character The {@link MorseCharacter character} to add.
     */
    void insertCharacter(MorseCharacter character);

    /**
     * Returns all the {@link MorseCharacter characters}.
     * @return A iterable with all the characters.
     */
    Iterable<MorseCharacter> getCharacters();

    /**
     * Decodes morse code into words.
     * @param code The morse code
     * @return The decoded word.
     */
    String decodeMorseCode(String code);

    /**
     * Creates a new bst of Morse code with only Letters.
     * @return The new {@link MorseCode code}.
     */
    MorseCode getOnlyLetterMorseCode();

    /**
     * Decodes morse code into words, but it can only decode letters.
     * @param code The morse code
     * @return The decoded word.
     */
    String decodeMorseCodeOnlyLetters(String code);

    /**
     * Codes words into morse code, but it can only code letters.
     * @param code The word.
     * @return The morse code of the word.
     */
    String codeMorseCodeOnlyLetters(String code);

    /**
     * Using the morse code finds the beginning sequence in common of the params.
     * @param code1 A morse code.
     * @param code2 Another morse code.
     * @return The beginning sequence in common of the params.
     */
    String getBeginningSequenceCommon(String code1, String code2);

    /**
     * Decodes the words and orders them by number of occurrences of the type of character.
     * @param morseWords A list of words in morse code.
     * @param type The type to order by the number of occurrences.
     * @return The decoded list of words in order.
     */
    LinkedList<String> orderListOfWordsByType(List<String> morseWords, MorseCharacter.Type type);
}
